# frozen_string_literal: true

module EE
  module Organizations
    module GroupsController
      def current_group
        @group
      end
    end
  end
end
